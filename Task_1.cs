﻿using System;

public interface IMinor
{
    double Value { get;}
    void Addition(IMinor minor);
    void Multiplication(IMinor minor);
    void Division(IMinor minor);
}

public interface IMatrix
{
    double[,] CurrentMatrix { get; set; }
    double this[int row, int col] { get; set; }
    IMinor GetMinorOfRank(int rank);
    IMinor GetMinorOfRank(int[] rowsIndex, int[] colsIndex);
    IMinor GetAdditionalMinorOfRank(int rank);
    IMinor GetAdditionalMinorOfRank(int[] rowsIndex, int[] colsIndex);
}

public interface ISquareMatrix : IMatrix
{
    IMinor GetMinorOfElement(int row, int column);
}
public class Minor : IMinor
{
    public double Value { get; private set; }
    public Minor(double value)
    {
        Value = value;
    }

    public void Addition(IMinor minor)
    {
        Value += minor.Value;
    }
    public void Multiplication(IMinor minor)
    {
         Value *= minor.Value;
    }

    public void Division(IMinor minor)
    {
         Value /= minor.Value;
    }
}

public abstract class AbstractMatrix : IMatrix
{
    // Set/Get matrix
    public abstract double[,] CurrentMatrix { get; set; }

    // Indexer for private value matrix
    public double this[int row, int col]
    {
        get { return CurrentMatrix[row, col]; }
        set { CurrentMatrix[row, col] = value; }
    }
    // Method return minor matriz
    protected double[,] GetMinorMatrix(double[,] matrix, int row, int column)
    {
        int size = matrix.GetLength(0);
        double[,] minorMatrix = new double[size - 1, size - 1];
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
            {
                if ((i != row) || (j != column))
                {
                    if (i > row && j < column) minorMatrix[i - 1, j] = matrix[i, j];
                    if (i < row && j > column) minorMatrix[i, j - 1] = matrix[i, j];
                    if (i > row && j > column) minorMatrix[i - 1, j - 1] = matrix[i, j];
                    if (i < row && j < column) minorMatrix[i, j] = matrix[i, j];
                }
            }
        return minorMatrix;
    }
    // Method return minor matriz
    protected double[,] GetMinorMatrix(double[,] matrix, int[] rows, int[] cols)
    {
        Array.Sort(rows);
        Array.Sort(cols);
        double[,] minorMatrix = new double[rows.GetLength(0), rows.GetLength(0)];
        int rowIndex = 0;
        for (int i = 0; i < CurrentMatrix.GetLength(0); i++)
        {
            bool isRowSelected = false;
            int colIndex = 0;
            for (int k = rowIndex; k < rows.GetLength(0); k++)
            {
                if (i == rows[k])
                {
                    isRowSelected = true;
                    break;
                }
            }
            if (!isRowSelected)
                continue;
            for (int j = 0; j < CurrentMatrix.GetLength(1); j++)
            {
                bool isColSelected = false;
                for (int m = colIndex; m < cols.GetLength(0); m++)
                {
                    if (j == cols[m])
                    {
                        isColSelected = true;
                        break;
                    }
                }
                if (isColSelected)
                {
                    minorMatrix[rowIndex, colIndex] = CurrentMatrix[i, j];
                    colIndex++;
                }
            }
            rowIndex++;
        }
        return minorMatrix;
    }
    // Method find determinant
    protected double GetDeterminantMatrix(double[,] matrix)
    {
        int rankMatrix = matrix.GetLength(0);
        if (rankMatrix == 1)
        {
            return matrix[0, 0];
        }
        if (rankMatrix == 2)
        {
            return matrix[0, 0]*matrix[1, 1] - matrix[0, 1]*matrix[1, 0];
        }
        double determinant = 0;
        for (int j = 0; j < rankMatrix; j++)
        {
            determinant += Math.Pow(-1, 0 + j)*matrix[0, j]*GetDeterminantMatrix(GetMinorMatrix(matrix, 0, j));
        }
        return determinant;
    }
    // Method return random minor for input rank
    public IMinor GetMinorOfRank(int rank)
    {
        if (rank > CurrentMatrix.GetLength(0) || rank > CurrentMatrix.GetLength(1))
        {
            throw new ArgumentException("The rank cannot be more than the number of columns or rows.");
        }
        int[] colsIndex = new int[rank];
        int[] rowsIndex = new int[rank];
        Random random = new Random();

        for (int i = 0; i < rank; i++)
        {
            rowsIndex[i] = random.Next(0, CurrentMatrix.GetLength(0) - 1);
            colsIndex[i] = random.Next(0, CurrentMatrix.GetLength(1) - 1);
        }


        return new Minor(GetDeterminantMatrix(GetMinorMatrix(CurrentMatrix, rowsIndex, colsIndex)));
    }
    // Method return minor for input rows and columns indexs
    public IMinor GetMinorOfRank(int[] rowsIndex, int[] colsIndex)
    {
        if (rowsIndex.Length != colsIndex.Length)
        {
            throw new ArgumentException("The number of elements in the arrays should be the same.");
        }
        int maxRank;
        if (CurrentMatrix.GetLength(0) > CurrentMatrix.GetLength(1))
        {
            maxRank = CurrentMatrix.GetLength(1);
        }
        else
        {
            maxRank = CurrentMatrix.GetLength(0);
        }
        if (maxRank < rowsIndex.Length || maxRank < colsIndex.Length)
        {
            throw new ArgumentException(
                "The number of elements in the arrays should not be more than the maximum rank of a matrix");
        }
        for (int i = 0; i < rowsIndex.Length; i++)
        {
            if (rowsIndex[i] > CurrentMatrix.GetLength(0))
                throw new ArgumentException("Incorrect row value in the array rowsIndex");
            if (colsIndex[i] > CurrentMatrix.GetLength(1))
                throw new ArgumentException("Incorrect column value in the array colsIndex");
        }
        return new Minor(GetDeterminantMatrix(GetMinorMatrix(CurrentMatrix, rowsIndex, colsIndex)));

    }
    // Method return summ rows and columns index for additional minor
    private int GetSummNumberOfColsAndRows(int[] rowsIndex, int[] colsIndex)
    {
        int summ = 0;
        for (int i = 0; i < rowsIndex.Length; i++)
        {
            summ += rowsIndex[i] + colsIndex[i];
        }
        return summ;
    }
    // Method return additional minor for matrix and input rank
    public IMinor GetAdditionalMinorOfRank(int rank)
    {
        if (rank > CurrentMatrix.GetLength(0) || rank > CurrentMatrix.GetLength(1))
        {
            throw new ArgumentException("The rank cannot be more than the number of columns or rows.");
        }
        int[] colsIndex = new int[rank];
        int[] rowsIndex = new int[rank];
        Random random = new Random();

        for (int i = 0; i < rank; i++)
        {
            rowsIndex[i] = random.Next(0, CurrentMatrix.GetLength(0) - 1);
            colsIndex[i] = random.Next(0, CurrentMatrix.GetLength(1) - 1);
        }
        double ratio = Math.Pow(-1, GetSummNumberOfColsAndRows(rowsIndex, colsIndex));
        return new Minor(ratio * GetDeterminantMatrix(GetMinorMatrix(CurrentMatrix, rowsIndex, colsIndex)));
    }
    // Method return additional minor for matrix and input rows and columns indexs
    public IMinor GetAdditionalMinorOfRank(int[] rowsIndex, int[] colsIndex)
    {
        double ratio = Math.Pow(-1, GetSummNumberOfColsAndRows(rowsIndex, colsIndex));
        return new Minor(ratio * GetMinorOfRank(rowsIndex, colsIndex).Value);
    }
}
// class simple matrix
public class Matrix : AbstractMatrix
{
    private double[,] _matrix;

    public override double[,] CurrentMatrix
    {
        get { return _matrix; }
        set { _matrix = value; }
    }

    public Matrix(int rowCount, int colCount)
    {
        CurrentMatrix = new double[rowCount, colCount];
    }

    public Matrix(double[,] matrix)
    {
        CurrentMatrix = matrix;
    }
}
// class square matrix
public class SquareMatrix : AbstractMatrix, ISquareMatrix
{
    private double[,] _matrix;

    public override double[,] CurrentMatrix
    {
        get { return _matrix; }
        set
        {
            if (value.GetLength(0) != value.GetLength(1))
            {
                throw new ArgumentException(
                    "The matrix must be square. The number of rows not equal to number of columns.");
            }
            _matrix = value;
        }
    }

    public SquareMatrix(int size)
    {
        CurrentMatrix = new double[size, size];
    }

    public SquareMatrix(double[,] matrix)
    {
        CurrentMatrix = matrix;
    }
    // method return minor matrix for input element index
    public IMinor GetMinorOfElement(int row, int column)
    {
        return new Minor(GetDeterminantMatrix(GetMinorMatrix(CurrentMatrix, row, column)));
    }
}

internal class Program
{
    private static void Main()
    {
        Console.WriteLine("___Task1___");
        Console.WriteLine();

        Console.WriteLine("**Create Matrix**");
        ISquareMatrix matrix1 = new SquareMatrix(new double[3, 3]
        {
            {-1,3,2},
            {9,0,-5},
            {4,-3,7},
        });
        IMatrix matrix2 = new Matrix(new double[7, 4]
        {
            {-1, 0, -3, 9},
            {2, 7, 14, 6},
            {15, -27, 18, 31},
            {0, 1, 19, 8},
            {0, -12, 20, 14},
            {5, 3, -21, 9},
            {23, -10, -5, 58}
        });
        int[] colsIndex = new int[] {0, 1, 3};
        int[] rowsIndex = new int[] {1, 3, 5};

        Console.WriteLine("**Get minor of rank **");
        IMinor minor1 = matrix2.GetMinorOfRank(rowsIndex,colsIndex);
        Console.WriteLine("Value minor1: {0}", minor1.Value);

        Console.WriteLine("**Get additional minor of rank **");
        IMinor minor2 = matrix2.GetAdditionalMinorOfRank(rowsIndex, colsIndex);
        Console.WriteLine("Value minor2: {0}", minor2.Value);

        Console.WriteLine("**Get minor of element**");
        IMinor minor3 = matrix1.GetMinorOfElement(0,1);
        Console.WriteLine("Value minor3: {0}", minor3.Value);

        Console.WriteLine("**Additional minors**");
        minor1.Addition(minor3);
        Console.WriteLine("Value minor1: {0}", minor1.Value);

        Console.WriteLine("**Division minors**");
        minor1.Division(minor3);
        Console.WriteLine("Value minor1: {0}", minor1.Value);

        Console.WriteLine("**Multiplication minors**");
        minor1.Multiplication(minor3);
        Console.WriteLine("Value minor1: {0}", minor1.Value);
    }
}

